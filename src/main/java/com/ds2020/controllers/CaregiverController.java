package com.ds2020.controllers;

import com.ds2020.dtos.*;
import com.ds2020.entities.MedicationPlan;
import com.ds2020.entities.Patient;
import com.ds2020.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver_home")
@PreAuthorize("hasRole('ROLE_CAREGIVER')")
public class CaregiverController {

    @Autowired
    PasswordEncoder encoder;

    CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") UUID id) {
        CaregiverDTO dto = caregiverService.findCaregiverById(id);
        for (Patient patient : dto.getListPersonCareOf()) {
            if (patient.getMedicationPlan() != null) {
                MedicationPlan medicationPlan = patient.getMedicationPlan();
                medicationPlan.setPatients(null);
                patient.setMedicationPlan(medicationPlan);
            }
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
