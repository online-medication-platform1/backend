package com.ds2020.controllers;

import com.ds2020.entities.MedicationPlan;
import com.ds2020.entities.Patient;
import com.ds2020.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient_home")
@PreAuthorize("hasRole('ROLE_PATIENT')")
public class PatientController {

    @Autowired
    PasswordEncoder encoder;

    PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Patient> getPatient(@PathVariable("id") UUID id) {
        System.out.println("Aici se ajunge");
        Patient dto = patientService.findPatientById(id);
        if (dto.getMedicationPlan() != null) {
            MedicationPlan medicationPlan = dto.getMedicationPlan();
            medicationPlan.setPatients(null);
            dto.setMedicationPlan(medicationPlan);
        }

        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}
