package com.ds2020.controllers;

import com.ds2020.dtos.*;
import com.ds2020.entities.*;
import com.ds2020.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
@PreAuthorize("hasRole('ROLE_DOCTOR')")
public class DoctorController {

    @Autowired
    PasswordEncoder encoder;

    DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/patients")
    public ResponseEntity<List<PatientDTO>> getPatients() {
        List<PatientDTO> dtos = doctorService.findAllPatient();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/caregivers")
    public ResponseEntity<List<CaregiverDTO>> getCaregiver() {
        List<CaregiverDTO> dtos = doctorService.findAllCaregiver();

        for (CaregiverDTO caregiverDTO : dtos) {
            for (Patient patient : caregiverDTO.getListPersonCareOf()) {
                System.out.println(patient.getMedicationPlan().getId());
                MedicationPlan medicationPlan = patient.getMedicationPlan();
                medicationPlan.setPatients(null);
                patient.setMedicationPlan(medicationPlan);
            }
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/medications")
    public ResponseEntity<List<MedicationDTO>> getMedication() {
        List<MedicationDTO> dtos = doctorService.findAllMedications();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/insert_patient")
    public ResponseEntity<?> registerPatient(@Valid @RequestBody PatientDTO patientDTO) {
        patientDTO.setPassword(encoder.encode(patientDTO.getPassword()));
        UUID id = doctorService.insertPatient(patientDTO);

        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PostMapping("/insert_caregiver")
    public ResponseEntity<?> registerCaregiver(@Valid @RequestBody AddCaregiverRequest addCaregiverRequest) {
        CaregiverDTO caregiverDTO = new CaregiverDTO(addCaregiverRequest.getId(), addCaregiverRequest.getEmail(), addCaregiverRequest.getPassword(), addCaregiverRequest.getName(), addCaregiverRequest.getBirthDate(), addCaregiverRequest.getGender(), addCaregiverRequest.getAddress(), addCaregiverRequest.getRole());
        caregiverDTO.setPassword(encoder.encode(caregiverDTO.getPassword()));
        UUID id = doctorService.insertCaregiver(caregiverDTO);

        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PostMapping("/insert_medication")
    public ResponseEntity<?> registerMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        UUID id = doctorService.insertMedication(medicationDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PostMapping("/add_patient_to_caregiver")
    public ResponseEntity<?> addPatientToCaregiver(@Valid @RequestBody AddPationCareofRequest addPationCareofRequest) {
        doctorService.add_patient_to_caregiver(addPationCareofRequest);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/add_medication_plan_to_patient")
    public ResponseEntity<?> addMedicationPlan(@Valid @RequestBody AddMedicationPlanRequest addMedicationPlanRequest) {
        UUID id = doctorService.addMedicationPlanToPatient(addMedicationPlanRequest);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping(value = "/update_patient")
    public ResponseEntity<PatientDTO> updatePerson(@RequestBody PatientDTO patientDTO) {
        PatientDTO dto = doctorService.updatePatient(patientDTO.getId(), patientDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/update_caregiver")
    public ResponseEntity<CaregiverDTO> updateCaregiver(@RequestBody AddCaregiverRequest addCaregiverRequest) {
        CaregiverDTO caregiverDTO = new CaregiverDTO(addCaregiverRequest.getId(), addCaregiverRequest.getEmail(), addCaregiverRequest.getPassword(), addCaregiverRequest.getName(), addCaregiverRequest.getBirthDate(), addCaregiverRequest.getGender(), addCaregiverRequest.getAddress(), addCaregiverRequest.getRole());
        CaregiverDTO dto = doctorService.updateCaregiver(caregiverDTO.getId(), caregiverDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PutMapping(value = "/update_medication")
    public ResponseEntity<MedicationDTO> updateMedication(@RequestBody MedicationDTO medicationDTO) {
        MedicationDTO dto = doctorService.updateMedication(medicationDTO.getId(), medicationDTO);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/remove_patient")
    public ResponseEntity<?> deletePatient(@RequestBody UUID id) {
        doctorService.delete_patient(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping(value = "/remove_caregiver")
    public ResponseEntity<?> deleteCaregiver(@RequestBody UUID id) {
        doctorService.delete_caregiver(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @DeleteMapping(value = "/remove_medication")
    public ResponseEntity<?> deleteMedication(@RequestBody UUID id) {
        doctorService.delete_medication(id);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
