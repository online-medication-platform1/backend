package com.ds2020.rmi.builders;

import com.ds2020.entities.Medication;
import com.ds2020.entities.MedicationPlan;
import com.ds2020.rmi.models.MedicationSimpleClass;

import java.util.ArrayList;
import java.util.List;

public class MedicationSimpleClassBuilder {

    public MedicationSimpleClassBuilder() {
    }

    public static List<MedicationSimpleClass> MedicationPlantoMedicationSimpleClass(MedicationPlan medicationPlan) {

        List<Medication> temporarListMedication = new ArrayList<>();
        for(Medication medication: medicationPlan.getMedicationsList())
            temporarListMedication.add(medication);

        List<MedicationSimpleClass> medicationSimpleClassList = new ArrayList<>();
        for(int i = 0; i<temporarListMedication.size(); i++){
            medicationSimpleClassList.add(new MedicationSimpleClass(temporarListMedication.get(i).getId(), temporarListMedication.get(i).getName(), medicationPlan.getIntakeInterval().get(i), temporarListMedication.get(i).getDosage()));
        }

        return medicationSimpleClassList;
    }
}
