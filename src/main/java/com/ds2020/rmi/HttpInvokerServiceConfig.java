package com.ds2020.rmi;

import com.ds2020.repositores.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class HttpInvokerServiceConfig {

    @Autowired
    PatientRepository patientRepository;

    @Bean(name = "/dispenser")
    HttpInvokerServiceExporter accountService() {
        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
        exporter.setService(new DispenserMedicationPlanServiceImpl(patientRepository));
        exporter.setServiceInterface(DispenserMedicationPlanService.class);
        return exporter;
    }
}
