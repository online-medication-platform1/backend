package com.ds2020.rmi;

import com.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.ds2020.entities.Patient;
import com.ds2020.repositores.PatientRepository;
import com.ds2020.rmi.builders.MedicationSimpleClassBuilder;
import com.ds2020.rmi.models.MedicationSimpleClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DispenserMedicationPlanServiceImpl implements DispenserMedicationPlanService {

    PatientRepository patientRepository;

    public DispenserMedicationPlanServiceImpl(PatientRepository patientRepository){
        this.patientRepository = patientRepository;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(DispenserMedicationPlanServiceImpl.class);

    @Override
    public List<MedicationSimpleClass> getMedicationPlan(String PatientID) {
        UUID uid = UUID.fromString(PatientID);
        if(patientRepository != null) {
            Optional<Patient> prosumerOptional = patientRepository.findById(uid);
            if (!prosumerOptional.isPresent()) {
                LOGGER.error("Patient with id {} was not found in db", uid);
                throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + uid + " not found");
            }
            return MedicationSimpleClassBuilder.MedicationPlantoMedicationSimpleClass(prosumerOptional.get().getMedicationPlan());
        }
        return null;
    }

    @Override
    public void medicationNotTaken(String patientID, String medication) {
        System.out.println("Patient with id=" + patientID + " forgot to take medication " + medication);
    }

    @Override
    public void medicationTaken(String patientID, String medication) {
        System.out.println("Patient with id=" + patientID + " taken correctly medication " + medication);

    }
}
