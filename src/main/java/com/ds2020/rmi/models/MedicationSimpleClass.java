package com.ds2020.rmi.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

public class MedicationSimpleClass implements Serializable {
    private UUID id;
    private String name;
    private String timeInterval;
    private String dosage;

    public MedicationSimpleClass(){
    }

    public MedicationSimpleClass(UUID id, String name, String timeInterval, String dosage) {
        this.id = id;
        this.name = name;
        this.timeInterval = timeInterval;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(String timeInterval) {
        this.timeInterval = timeInterval;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
