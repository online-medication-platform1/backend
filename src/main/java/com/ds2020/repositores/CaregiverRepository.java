package com.ds2020.repositores;

import com.ds2020.entities.Caregiver;
import com.ds2020.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CaregiverRepository extends JpaRepository<Caregiver, UUID> {

}
