package com.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class AddPationCareofRequest {

    @NotNull
    private UUID patientID;
    @NotNull
    private UUID caregiverID;

    public AddPationCareofRequest() {
    }

    public AddPationCareofRequest(@NotNull UUID patientID, @NotNull UUID caregiverID) {
        this.patientID = patientID;
        this.caregiverID = caregiverID;
    }

    public UUID getPatientID() {
        return patientID;
    }

    public void setPatientID(UUID patientID) {
        this.patientID = patientID;
    }

    public UUID getCaregiverID() {
        return caregiverID;
    }

    public void setCaregiverID(UUID caregiverID) {
        this.caregiverID = caregiverID;
    }
}
