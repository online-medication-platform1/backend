package com.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.UUID;

public class MedicationDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private ArrayList<String> listOfSideEffects;
    @NotNull
    private String dosage;

    public MedicationDTO() {

    }

    public MedicationDTO(UUID id, @NotNull String name, @NotNull ArrayList<String> listOfSideEffects, @NotNull String dosage) {
        this.id = id;
        this.name = name;
        this.listOfSideEffects = listOfSideEffects;
        this.dosage = dosage;
    }

    public MedicationDTO(@NotNull String name, @NotNull ArrayList<String> listOfSideEffects, @NotNull String dosage) {
        this.name = name;
        this.listOfSideEffects = listOfSideEffects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getListOfSideEffects() {
        return listOfSideEffects;
    }

    public void setListOfSideEffects(ArrayList<String> listOfSideEffects) {
        this.listOfSideEffects = listOfSideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
