package com.ds2020.dtos.builders;

import com.ds2020.dtos.PatientDTO;
import com.ds2020.dtos.UserDTO;
import com.ds2020.entities.Patient;
import com.ds2020.entities.User;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        return new PatientDTO(patient.getId(), patient.getEmail(), patient.getPassword(), patient.getName(), patient.getBirthDate(), patient.getGender(), patient.getAddress(), patient.getRole(), patient.getMedicalRecord());
    }

    public static Patient toEntityPatient(PatientDTO patientDTO) {
        return new Patient(patientDTO.getEmail(), patientDTO.getPassword(), patientDTO.getName(), patientDTO.getBirthDate(), patientDTO.getGender(), patientDTO.getAddress(), patientDTO.getRole(), patientDTO.getMedicalRecord());
    }
}

