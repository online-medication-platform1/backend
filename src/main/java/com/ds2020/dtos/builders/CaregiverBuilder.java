package com.ds2020.dtos.builders;


import com.ds2020.dtos.CaregiverDTO;
import com.ds2020.entities.Caregiver;
import com.ds2020.entities.Patient;

import java.util.HashSet;
import java.util.Set;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getEmail(), caregiver.getPassword(), caregiver.getName(), caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress(), caregiver.getRole(), new HashSet<>(caregiver.getListPersonCareOf()));
    }

    public static Caregiver toEntityCaregiver(CaregiverDTO caregiverDTO) {
        return new Caregiver(caregiverDTO.getEmail(), caregiverDTO.getPassword(), caregiverDTO.getName(), caregiverDTO.getBirthDate(), caregiverDTO.getGender(), caregiverDTO.getAddress(), caregiverDTO.getRole());
    }

}
