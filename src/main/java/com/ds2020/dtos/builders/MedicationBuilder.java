package com.ds2020.dtos.builders;

import com.ds2020.dtos.MedicationDTO;
import com.ds2020.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId(), medication.getName(), medication.getListOfSideEffects(), medication.getDosage());
    }

    public static Medication toEntityMedication(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getName(), medicationDTO.getListOfSideEffects(), medicationDTO.getDosage());
    }
}
