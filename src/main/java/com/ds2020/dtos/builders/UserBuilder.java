package com.ds2020.dtos.builders;

import com.ds2020.dtos.UserDTO;
import com.ds2020.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO toUserDTO(User user) {
        return new UserDTO(user.getId(), user.getEmail(), user.getPassword(), user.getName(), user.getBirthDate(), user.getGender(), user.getAddress(), user.getRole());
    }

    public static User toEntityUser(UserDTO userDTO) {
        return new User(userDTO.getEmail(), userDTO.getPassword(), userDTO.getName(), userDTO.getBirthDate(), userDTO.getGender(), userDTO.getAddress(), userDTO.getRole());
    }
}
