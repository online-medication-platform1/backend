package com.ds2020.dtos;

import java.util.ArrayList;
import java.util.UUID;

public class AddMedicationPlanRequest {

    private ArrayList<UUID> medicationIdList;
    private ArrayList<String> intakeIntervalList;
    private String periodTreatment;
    private UUID patientId;

    public AddMedicationPlanRequest(){

    }

    public AddMedicationPlanRequest(ArrayList<UUID> medicationIdList, ArrayList<String> intakeIntervalList, String periodTreatment, UUID patientId) {
        this.medicationIdList = medicationIdList;
        this.intakeIntervalList = intakeIntervalList;
        this.periodTreatment = periodTreatment;
        this.patientId = patientId;
    }

    public ArrayList<UUID> getMedicationIdList() {
        return medicationIdList;
    }

    public void setMedicationIdList(ArrayList<UUID> medicationIdList) {
        this.medicationIdList = medicationIdList;
    }

    public ArrayList<String> getIntakeIntervalList() {
        return intakeIntervalList;
    }

    public void setIntakeIntervalList(ArrayList<String> intakeIntervalList) {
        this.intakeIntervalList = intakeIntervalList;
    }

    public String getPeriodTreatment() {
        return periodTreatment;
    }

    public void setPeriodTreatment(String periodTreatment) {
        this.periodTreatment = periodTreatment;
    }

    public UUID getPatientId() {
        return patientId;
    }

    public void setPatientId(UUID patientId) {
        this.patientId = patientId;
    }
}
