package com.ds2020.dtos;

import com.ds2020.entities.ERole;
import com.ds2020.entities.Patient;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.*;

public class CaregiverDTO {

    private UUID id;
    @NotNull
    private String email;
    @NotNull
    private String password;
    @NotNull
    private String name;
    @NotNull
    @JsonFormat(pattern = "dd/MM/yy")
    private Date birthDate;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    private ERole role;

    private Set<Patient> listPersonCareOf = new HashSet<Patient>();

    public CaregiverDTO() {
    }

    public CaregiverDTO(UUID id, @NotNull String email, @NotNull String password, @NotNull String name, @NotNull Date birthDate, @NotNull String gender, @NotNull String address, @NotNull ERole role, HashSet<Patient> listPersonCareOf) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
        this.listPersonCareOf = listPersonCareOf;
    }

    public CaregiverDTO(@NotNull String email, @NotNull String password, @NotNull String name, @NotNull Date birthDate, @NotNull String gender, @NotNull String address, @NotNull ERole role) {
        this.email = email;
        this.password = password;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
    }

    public CaregiverDTO(UUID id, @NotNull String email, @NotNull String password, @NotNull String name, @NotNull Date birthDate, @NotNull String gender, @NotNull String address, @NotNull ERole role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ERole getRole() {
        return role;
    }

    public void setRole(ERole role) {
        this.role = role;
    }

    public Set<Patient> getListPersonCareOf() {
        return listPersonCareOf;
    }

    public void setListPersonCareOf(HashSet<Patient> listPersonCareOf) {
        this.listPersonCareOf = listPersonCareOf;
    }
}
