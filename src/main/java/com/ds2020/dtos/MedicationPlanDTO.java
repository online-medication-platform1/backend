package com.ds2020.dtos;

import com.ds2020.entities.Medication;
import com.ds2020.entities.Patient;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;
    private Set<Medication> medicationsList;
    private HashMap<Medication, String> intakeInterval;
    private String periodTreatment;
    private Set<Patient> patients = new HashSet<>();

    public MedicationPlanDTO() {
    }

    public MedicationPlanDTO(UUID id, Set<Medication> medicationsList, HashMap<Medication, String> intakeInterval, String periodTreatment, Set<Patient> patients) {
        this.id = id;
        this.medicationsList = medicationsList;
        this.intakeInterval = intakeInterval;
        this.periodTreatment = periodTreatment;
        this.patients = patients;
    }

    public MedicationPlanDTO(Set<Medication> medicationsList, HashMap<Medication, String> intakeInterval, String periodTreatment, Set<Patient> patients) {
        this.medicationsList = medicationsList;
        this.intakeInterval = intakeInterval;
        this.periodTreatment = periodTreatment;
        this.patients = patients;
    }
}
