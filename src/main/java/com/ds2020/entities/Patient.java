package com.ds2020.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
public class Patient extends User implements Serializable {

    @Column(name = "medicalRecord", nullable = false)
    private String medicalRecord;

    @ManyToOne
    @JoinColumn(name = "medicationPlan_id")
    private MedicationPlan medicationPlan;

    public Patient(){
    }

    public Patient(String email, String password, String name, Date birthDate, String gender, String address, ERole role, String medicalRecord) {
        super(email, password, name, birthDate, gender, address, role);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
