package com.ds2020.entities;

public enum ERole {
    ROLE_PATIENT,
    ROLE_DOCTOR,
    ROLE_CAREGIVER
}
