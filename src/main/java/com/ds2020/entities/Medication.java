package com.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class Medication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "listOfSideEffects", nullable = false)
    private ArrayList<String> listOfSideEffects;

    @Column(name = "dosage", nullable = false)
    private String dosage;

    public Medication() {
    }

    public Medication(String name, ArrayList<String> listOfSideEffects, String dosage) {
        this.name = name;
        this.listOfSideEffects = listOfSideEffects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getListOfSideEffects() {
        return listOfSideEffects;
    }

    public void setListOfSideEffects(ArrayList<String> listOfSideEffects) {
        this.listOfSideEffects = listOfSideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
}
