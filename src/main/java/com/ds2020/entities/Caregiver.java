package com.ds2020.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Caregiver extends User implements Serializable {

    @Column(name = "listPersonCareOf", nullable = true)
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Patient> listPersonCareOf;

    public Caregiver() {
    }

    public Caregiver(String email, String password, String name, Date birthDate, String gender, String address, ERole role, HashSet<Patient> listPersonCareOf) {
        super(email, password, name, birthDate, gender, address, role);
        this.listPersonCareOf = listPersonCareOf;
    }

    public Caregiver(String email, String password, String name, Date birthDate, String gender, String address, ERole role) {
        super(email, password, name, birthDate, gender, address, role);
        listPersonCareOf = new HashSet<>();
    }

    public Set<Patient> getListPersonCareOf() {
        return listPersonCareOf;
    }

    public void setListPersonCareOf(Set<Patient> listPersonCareOf) {
        this.listPersonCareOf = listPersonCareOf;
    }
}
