package com.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class MedicationPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @OneToMany(fetch = FetchType.EAGER)
    private Set<Medication> medicationsList;

    @Column
    private ArrayList<String> intakeInterval;

    @Column
    private String periodTreatment;

    @OneToMany(mappedBy = "medicationPlan" /*, fetch= FetchType.EAGER*/)
    private Set<Patient> patients = new HashSet<>();

    public MedicationPlan() {
    }

    public MedicationPlan(Set<Medication> medicationsList, ArrayList<String> intakeInterval, String periodTreatment, Set<Patient> patientSet) {
        this.medicationsList = medicationsList;
        this.intakeInterval = intakeInterval;
        this.periodTreatment = periodTreatment;
        this.patients = patientSet;
    }

    public MedicationPlan(Set<Medication> medicationsList, ArrayList<String> intakeInterval, String periodTreatment) {
        this.medicationsList = medicationsList;
        this.intakeInterval = intakeInterval;
        this.periodTreatment = periodTreatment;
    }

    public MedicationPlan(String periodTreatment) {
        this.medicationsList = new LinkedHashSet<>();
        this.intakeInterval = new ArrayList<>();
        this.periodTreatment = periodTreatment;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Set<Medication> getMedicationsList() {
        return medicationsList;
    }

    public void setMedicationsList(Set<Medication> medicationsList) {
        this.medicationsList = medicationsList;
    }

    public ArrayList<String> getIntakeInterval() {
        return intakeInterval;
    }

    public void setIntakeInterval(ArrayList<String> intakeInterval) {
        this.intakeInterval = intakeInterval;
    }

    public String getPeriodTreatment() {
        return periodTreatment;
    }

    public void setPeriodTreatment(String periodTreatment) {
        this.periodTreatment = periodTreatment;
    }

    public Set<Patient> getPatients() {
        return patients;
    }

    public void setPatients(Set<Patient> patients) {
        this.patients = patients;
    }
}
