package com.ds2020.entities;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Doctor extends User implements Serializable {

    public Doctor() {
    }

    public Doctor(String email, String password, String name, Date birthDate, String gender, String address, ERole role) {
        super(email, password, name, birthDate, gender, address, role);
    }
}
