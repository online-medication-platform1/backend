package com.ds2020.services;

import com.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.ds2020.dtos.CaregiverDTO;
import com.ds2020.dtos.PatientDTO;
import com.ds2020.dtos.builders.CaregiverBuilder;
import com.ds2020.entities.Caregiver;
import com.ds2020.entities.MedicationPlan;
import com.ds2020.entities.MonitoredData;
import com.ds2020.repositores.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class CaregiverService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final CaregiverRepository caregiverRepository;


    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }
}
