package com.ds2020.services;

import com.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.ds2020.dtos.*;
import com.ds2020.dtos.builders.CaregiverBuilder;
import com.ds2020.dtos.builders.MedicationBuilder;
import com.ds2020.dtos.builders.PatientBuilder;
import com.ds2020.dtos.builders.UserBuilder;
import com.ds2020.entities.*;
import com.ds2020.repositores.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;


    @Autowired
    public DoctorService(UserRepository userRepository, PatientRepository patientRepository, CaregiverRepository caregiverRepository, MedicationRepository medicationRepository, MedicationPlanRepository medicationPlanRepository) {
        this.userRepository = userRepository;
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
        this.medicationRepository = medicationRepository;
        this.medicationPlanRepository = medicationPlanRepository;

    }

    /*public List<PatientDTO> findByRole(ERole eRole) {
        List<Patient> userList = null;
        switch (eRole){
            case ROLE_DOCTOR:
                userList = userRepository.findByRole(ERole.ROLE_DOCTOR);
                break;
            case ROLE_PATIENT:
                userList = userRepository.findByRole(ERole.ROLE_PATIENT);
                break;
            case ROLE_CAREGIVER:
                userList = userRepository.findByRole(ERole.ROLE_CAREGIVER);
                break;
        }
        return userList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }*/

    public List<PatientDTO> findAllPatient() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream().map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public List<CaregiverDTO> findAllCaregiver() {
        List<Caregiver> caregivertList = caregiverRepository.findAll();
        return caregivertList.stream().map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public List<MedicationDTO> findAllMedications() {
        List<Medication> medicationsList = medicationRepository.findAll();
        return medicationsList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationPlan findMedicationPlanById(UUID id) {
        Optional<MedicationPlan> prosumerOptional = medicationPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(PatientDTO.class.getSimpleName() + " with id: " + id);
        }
        return prosumerOptional.get();
    }

    public UUID insertPatient(PatientDTO patientDTO) {
        Patient patient = PatientBuilder.toEntityPatient(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public UUID insertCaregiver(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = CaregiverBuilder.toEntityCaregiver(caregiverDTO);
        caregiver = userRepository.save(caregiver);
        LOGGER.debug("Patient with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public UUID insertMedication(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntityMedication(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Patient with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    public PatientDTO updatePatient(UUID id, PatientDTO patientDTO) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(PatientDTO.class.getSimpleName() + " with id: " + id);
        }
        Patient patient = prosumerOptional.get();
        patient.setEmail(patientDTO.getEmail());
        patient.setPassword(patientDTO.getPassword());
        patient.setName(patientDTO.getName());
        patient.setBirthDate(patientDTO.getBirthDate());
        patient.setGender(patientDTO.getGender());
        patient.setAddress(patientDTO.getAddress());
        patient.setRole(patientDTO.getRole());

        Patient patientUpdate = userRepository.save(patient);

        return PatientBuilder.toPatientDTO(patientUpdate);
    }

    public CaregiverDTO updateCaregiver(UUID id, CaregiverDTO caregiverDTO) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(CaregiverDTO.class.getSimpleName() + " with id: " + id);
        }
        Caregiver caregiver = prosumerOptional.get();
        caregiver.setEmail(caregiverDTO.getEmail());
        caregiver.setPassword(caregiverDTO.getPassword());
        caregiver.setName(caregiverDTO.getName());
        caregiver.setBirthDate(caregiverDTO.getBirthDate());
        caregiver.setGender(caregiverDTO.getGender());
        caregiver.setAddress(caregiverDTO.getAddress());
        caregiver.setRole(caregiverDTO.getRole());

        Caregiver caregiverUpdate = caregiverRepository.save(caregiver);

        return CaregiverBuilder.toCaregiverDTO(caregiverUpdate);
    }

    public MedicationDTO updateMedication(UUID id, MedicationDTO medicationDTO) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        Medication medication = prosumerOptional.get();
        medication.setName(medicationDTO.getName());
        medication.setDosage(medicationDTO.getDosage());

        Medication medicationUpdate = medicationRepository.save(medication);

        return MedicationBuilder.toMedicationDTO(medicationUpdate);
    }

    public void delete_patient(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        patientRepository.deleteById(id);
    }

    public void delete_caregiver(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        caregiverRepository.deleteById(id);
    }

    public void delete_medication(UUID id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        medicationRepository.deleteById(id);
    }

    public void add_medication_plan(MedicationPlanDTO medicationPlanDTO) {

    }

    public void add_patient_to_caregiver(AddPationCareofRequest addPationCareofRequest) {
        Optional<Caregiver> prosumerOptionalCaregiver = caregiverRepository.findById(addPationCareofRequest.getCaregiverID());
        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(addPationCareofRequest.getPatientID());
        if (!prosumerOptionalCaregiver.isPresent() || !prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient/Caregiver was not found in db");
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName());
        }
        Caregiver caregiver = prosumerOptionalCaregiver.get();
        Patient patient = prosumerOptionalPatient.get();
        Set<Patient> listPersonCareOf = caregiver.getListPersonCareOf();
        listPersonCareOf.add(patient);
        caregiver.setListPersonCareOf(listPersonCareOf);
        caregiverRepository.save(caregiver);
    }

    public UUID addMedicationPlanToPatient(AddMedicationPlanRequest addMedicationPlanRequest) {
        Set<Medication> medicationSet = new LinkedHashSet<>();
        for (UUID medicationID : addMedicationPlanRequest.getMedicationIdList()) {
            Optional<Medication> prosumerOptionalMedication = medicationRepository.findById(medicationID);
            if (!prosumerOptionalMedication.isPresent()) {
                LOGGER.error("Medication with id =" + medicationID + "was not found in db");
                throw new ResourceNotFoundException(Medication.class.getSimpleName());
            }
            Medication medication = prosumerOptionalMedication.get();
            medicationSet.add(medication);
        }

        Optional<Patient> prosumerOptionalPatient = patientRepository.findById(addMedicationPlanRequest.getPatientId());
        if (!prosumerOptionalPatient.isPresent()) {
            LOGGER.error("Patient with id=" + addMedicationPlanRequest.getPatientId() + "was not found in db");
            throw new ResourceNotFoundException(Patient.class.getSimpleName());
        }
        Set<Patient> patients = new HashSet<>();
        Patient patient = prosumerOptionalPatient.get();
        patients.add(patient);

        MedicationPlan medicationPlan = new MedicationPlan(medicationSet, addMedicationPlanRequest.getIntakeIntervalList(), addMedicationPlanRequest.getPeriodTreatment(), patients);

        patient.setMedicationPlan(medicationPlan);

        MedicationPlan medPlan = medicationPlanRepository.save(medicationPlan);
        patientRepository.save(patient);

        return medPlan.getId();
    }


}
