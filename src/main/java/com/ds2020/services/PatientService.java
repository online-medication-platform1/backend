package com.ds2020.services;

import com.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.ds2020.dtos.CaregiverDTO;
import com.ds2020.dtos.PatientDTO;
import com.ds2020.dtos.builders.CaregiverBuilder;
import com.ds2020.dtos.builders.PatientBuilder;
import com.ds2020.entities.Caregiver;
import com.ds2020.entities.MedicationPlan;
import com.ds2020.entities.Patient;
import com.ds2020.repositores.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public Patient findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id + " not found");
        }
        return prosumerOptional.get();
    }
}
