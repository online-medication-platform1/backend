package com.ds2020.services;

import com.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import com.ds2020.dtos.UserDTO;
import com.ds2020.dtos.builders.UserBuilder;
import com.ds2020.entities.User;
import com.ds2020.repositores.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> findUsers() {
        List<User> userList = userRepository.findAll();
        return userList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDTO findUserById(UUID id) {
        Optional<User> prosumerOptional = userRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        return UserBuilder.toUserDTO(prosumerOptional.get());
    }

    public UUID insert(UserDTO userDTO) {
        User user = UserBuilder.toEntityUser(userDTO);
        user = userRepository.save(user);
        LOGGER.debug("User with id {} was inserted in db", user.getId());
        return user.getId();
    }

    public UserDTO update(UUID id, UserDTO UserDTO) {
        Optional<User> prosumerOptional = userRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        User user = prosumerOptional.get();
        user.setEmail(UserDTO.getEmail());
        user.setPassword(UserDTO.getPassword());
        user.setName(UserDTO.getName());
        user.setBirthDate(UserDTO.getBirthDate());
        user.setGender(UserDTO.getGender());
        user.setAddress(UserDTO.getAddress());
        user.setRole(UserDTO.getRole());

        User userUpdated = userRepository.save(user);

        return UserBuilder.toUserDTO(userUpdated);
    }

    public void delete(UUID id) {
        Optional<User> prosumerOptional = userRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("User with id {} was not found in db", id);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + id);
        }
        userRepository.deleteById(id);
    }
}
