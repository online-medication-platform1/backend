package com.ds2020.services;

import com.ds2020.entities.MonitoredData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class MonitoredDataService {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    public void sendDataMonitored(MonitoredData monitoredData){
        simpMessagingTemplate.convertAndSend("/topic/monitoredData", monitoredData);
    }
}
