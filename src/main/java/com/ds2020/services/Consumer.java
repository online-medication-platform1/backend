package com.ds2020.services;

import com.ds2020.entities.MonitoredData;
import com.ds2020.rabbitmq.RabbitMQConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.concurrent.CountDownLatch;

@Service
public class Consumer {

    static Logger logger = LoggerFactory.getLogger(Consumer.class);
    private CountDownLatch latch = new CountDownLatch(1);

    @Autowired
    MonitoredDataService monitoredDataService;

    @RabbitListener(queues = RabbitMQConfiguration.QUEUE_NAME, containerFactory = "listenerContainerFactory")
    public void receiveMessage(final MonitoredData monitoredData) {
        this.rules(monitoredData);
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void rules(MonitoredData monitoredData){
        if(monitoredData.getActivity().equals("Sleeping")){
            long sleepPreioad = monitoredData.getEnd().getTime() - monitoredData.getStart().getTime();
            if(sleepPreioad >= 7 * 60 * 60 * 1000){
                logger.info("Sleep period longer than 7 hours " + monitoredData.getStart() + "-" + monitoredData.getEnd());
                monitoredDataService.sendDataMonitored(monitoredData);

            }
        }

        if(monitoredData.getActivity().equals("Leaving")){
            long leavingPreioad = monitoredData.getEnd().getTime() - monitoredData.getStart().getTime();
            if(leavingPreioad >= 5 * 60 * 60 * 1000){
                logger.info("The leaving activity (outdoor) is longer than 5 hours" + monitoredData.getStart() + "-" + monitoredData.getEnd());
                monitoredDataService.sendDataMonitored(monitoredData);


            }
        }

        if(monitoredData.getActivity().equals("Toileting") || monitoredData.getActivity().equals("Showering")){
            long bathPreioad = monitoredData.getEnd().getTime() - monitoredData.getStart().getTime();
            if(bathPreioad >= 30 * 60 * 1000){
                logger.info("Period spent in bathroom is longer than 30 minutes" + monitoredData.getStart() + "-" + monitoredData.getEnd());
                monitoredDataService.sendDataMonitored(monitoredData);

            }
        }
    }
}
