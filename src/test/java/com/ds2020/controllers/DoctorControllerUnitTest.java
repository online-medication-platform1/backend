package com.ds2020.controllers;

import com.ds2020.Ds2020Application;
import com.ds2020.Ds2020ApplicationTests;
import com.ds2020.dtos.CaregiverDTO;
import com.ds2020.dtos.LoginRequest;
import com.ds2020.dtos.MedicationDTO;
import com.ds2020.dtos.PatientDTO;
import com.ds2020.entities.ERole;
import com.ds2020.services.DoctorService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DoctorControllerUnitTest extends Ds2020ApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    PasswordEncoder encoder;

    @MockBean
    private DoctorService service;

    @Test
    public void insertPatientTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        /*LoginRequest loginRequest = new LoginRequest("test@yahoo.com", "1234");
        MvcResult result = mockMvc.perform(post("/sigin")
                        .content(objectMapper.writeValueAsString(loginRequest))
                        .contentType("application/json"))
                        .andExpect(status().isOk()).andReturn();

        String response = result.getResponse().getContentAsString();
        System.out.println(response);*/

        PatientDTO patientDTO = new PatientDTO("patient_test@yahoo.com", "1234", "Test", new Date("12/12/1008"), "M", "Obs", ERole.ROLE_PATIENT, "test");

        mockMvc.perform(post("/doctor/insert_patient")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void insertCaregiverTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CaregiverDTO caregiverDTO = new CaregiverDTO("caregiver_test@yahoo.com", encoder.encode("1234"), "Test", new Date("12/12/1008"), "M", "Obs", ERole.ROLE_CAREGIVER);

        mockMvc.perform(post("/doctor/insert_caregiver")
                .content(objectMapper.writeValueAsString(caregiverDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void insertMedicationTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        MedicationDTO medicationDTO = new MedicationDTO("Paracetamol", new ArrayList<String>(Collections.singleton(new String("test"))), "test");

        mockMvc.perform(post("/doctor/insert_medication")
                .content(objectMapper.writeValueAsString(medicationDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void insertPatientTestFailsDueToNull() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PatientDTO patientDTO = new PatientDTO(null, encoder.encode("1234"), "Test", new Date("12/12/1008"), "M", "Obs", ERole.ROLE_PATIENT, "test");

        mockMvc.perform(post("/doctor/insert_patient")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void updatePatients() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PatientDTO patientDTO = new PatientDTO(null, encoder.encode("1234"), "Test", new Date("12/12/1008"), "M", "Obs", ERole.ROLE_PATIENT, "test");

        mockMvc.perform(post("/doctor/update_patient")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void updateCaregiver() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PatientDTO patientDTO = new PatientDTO(null, encoder.encode("1234"), "Test", new Date("12/12/1008"), "M", "Obs", ERole.ROLE_PATIENT, "test");

        mockMvc.perform(post("/doctor/update_caregiver")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void updateMedication() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        PatientDTO patientDTO = new PatientDTO(null, encoder.encode("1234"), "Test", new Date("12/12/1008"), "M", "Obs", ERole.ROLE_PATIENT, "test");

        mockMvc.perform(post("/doctor/update_medication")
                .content(objectMapper.writeValueAsString(patientDTO))
                .contentType("application/json"))
                .andExpect(status().isUnauthorized());
    }

}
