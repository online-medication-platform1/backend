Build and execution consideration
1. In application.properties se modifica optiunea spring.jpa.hibernate.ddl-auto=create-drop pentru prima rulare apoi se modifica la validate
2. Pentru a introduce un utilizator cu role de doctor se face requestul urmator din Postman
https://ds2020-lucian-spring1.herokuapp.com/auth/signup
Content-Type --- application/json
{
    "email": "exempludoctor@yahoo.com",
    "password": "1234",
    "name": "exemplu",
    "birthDate": "03/21/1998",
    "gender": "M",
    "address": "Observatorului",
    "role": "ROLE_DOCTOR"
}
3. Pentru a intoduce alti utilizator putem utiliza applicatia web cu userul creat